from tableau import tableau
import pytest

@pytest.fixture
def tableauIns():
 return tableau([1,8,55,66,3])

@pytest.mark.max_test
def test_max(tableauIns):
 assert tableauIns.maximum()==66
 
@pytest.mark.indice_test
def test_indice(tableauIns):
 assert tableauIns.indice()==3


 
